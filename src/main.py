from fastapi import FastAPI
from src.controllers import vote_controller

app = FastAPI()

app.include_router(vote_controller.router)