from fastapi import APIRouter
from src.services.vote_service import vote_service_root

router = APIRouter()

@router.get("/")
def root():
    return vote_service_root()