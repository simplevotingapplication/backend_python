python -m venv app_env
app_env\Scripts\activate.ps1
pip install -r requirements.txt
uvicorn src.main.main:app --port 9001
deactivate